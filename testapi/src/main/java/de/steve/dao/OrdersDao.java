package de.steve.dao;

import java.util.List;

import de.steve.entity.Order;

public interface OrdersDao {

	
	
	
	public Order AddOrder(Order order) throws DoaException;
	
	public Order  FindAllOrder() throws DoaException;
	
	//public List<Order> findAll(int customer_id)throws DoaException;
	public List<Order> findAll(int customer_id)throws DoaException;
	
}
