package de.steve.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import de.steve.entity.Order;
import de.steve.utils.ConnectPostgres;

public class jdbcOrdersDao implements OrdersDao {

	@Override
	public Order AddOrder(Order order) throws DoaException {
		//String sql = "insert into Orders ( price, product_id ) VALUES (?,?);";
		String sql = "insert into Orders (id, price ) VALUES (?,?);";
		
		try(
				Connection con = ConnectPostgres.getConnection();
				PreparedStatement statement = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
				) {
			
			statement.setFloat(1, order.getId());
			statement.setFloat(2, order.getPrice());
			//statement.setLong(2,  order.getProductId());
			
			statement.executeUpdate();
			ResultSet keys = statement.getGeneratedKeys();
			keys.next();
			order.setId(keys.getInt(1));
			return order;
			
			
		}catch (Exception e) {
		throw new DoaException(e);
		}
		
	}
	
	@Override
	public Order FindAllOrder() throws DoaException {
		
		return null;
	}

	@Override
	public List<Order> findAll(int customer_Id) throws DoaException {
		
		//String sql =" select * from Orders";
		List<Order> list = new ArrayList<>();
		
		String sql =" select Orders.id as id,  ProductS.id as productid, ProductS.Title as producttitle, ProductS.Price as price, ProductS.Quantity as quantity from Customers join Orders on Orders.id= Customers.orderid join ProductS on ProductS.id = Orders.product_id where Customers.id= "+customer_Id+" ;";    
 
	
		try (Connection con = ConnectPostgres.getConnection();
				PreparedStatement statement = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
				ResultSet rs = statement.executeQuery();
				){
		
		//statement.setLong(1, customer_Id);
			while(rs.next()) {
				Order order = new Order();
				order.setId(rs.getInt("id"));
				order.setPrice(rs.getFloat("price"));
				order.setProductId(rs.getInt("productid"));
				order.setTitel(rs.getString("producttitle"));
				order.setQuantity(rs.getInt("quantity"));
				
				list.add(order);
				
				
			}
		}catch (Exception e) {
			throw new DoaException(e);
		
		}
		
		return list;
	}

}
