package de.steve.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import de.steve.entity.Order;

public final class ConnectPostgres {

	private static String host,port,db_name, username,password;
	
	//defauft constructor
	private ConnectPostgres() {	
		
	}
	
	static {host="localhost";
	         port="5432";
	        // db_name="postgres";
	         db_name="LupiterDB";
	         username="postgres";
	         password="root";
	};
	
    
	public static Connection getConnection() throws ClassNotFoundException,SQLException {
		
		
		 Class.forName("org.postgresql.Driver");
		 
		
		return DriverManager.getConnection("jdbc:postgresql://"+host+":"+port+"/"+db_name+"", 
				""+username+"", ""+password+"");
	}
	
     
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		
		 String sql =" select * from Orders";
     Connection con = getConnection();
     PreparedStatement statement = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
     
    
     ResultSet rs = statement.executeQuery();
     while(rs.next()) {
			//Order order = new Order();
			//order.setId(rs.getInt("id"));
			System.out.println(rs.getInt("id"));
			System.out.println(rs.getFloat("price"));
			System.out.println(rs.getInt("product_id"));
     }
     
     if(con !=null) {
    	 System.out.println("ok");
    	 
     }else {
    	 System.out.println("failed");
     }
    
  
	}

	
	
	
}
