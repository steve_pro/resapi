package de.steve.dao;

public final class Daofactory {
	
	
	private static final String IMPL_TYPE="jdbc";
	
	private Daofactory() {
		
	}

	
	public static OrdersDao getOrdersDao() throws DoaException {
		switch (IMPL_TYPE) {
		case "jdbc":
			return new jdbcOrdersDao();
		default:
			throw new DoaException("no suitable implementation available");
		}
		
		
	}
}
