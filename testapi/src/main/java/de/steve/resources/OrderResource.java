package de.steve.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import de.steve.dao.Daofactory;
import de.steve.dao.DoaException;
import de.steve.dao.OrdersDao;
import de.steve.entity.Order;

@Path("/orders")
public class OrderResource {

	private OrdersDao dao;
	
	public OrderResource() throws DoaException {
		dao = Daofactory.getOrdersDao();
	}
	@GET
	@Path("/{customers_id}")
	@Produces({"application/json"})
	public Response Getall(@PathParam("customers_id") Integer id) throws DoaException {
		return Response.ok(dao.findAll(id)).build();
		
	}
	
	@POST
	@Produces({"application/json"})
	@Consumes({"application/json"})
	public Response AddNewOrder(Order o) throws DoaException {
		
		 return Response.ok(dao. AddOrder(o)).build();
		
	}
}
